<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 2.5.2015
 * Time: 20:56
 */

namespace Retrech\Support;


use Patchwork\TurkishUtf8;
use Patchwork\Utf8;
use Illuminate\Support\Str as BaseHelper;

class Str extends BaseHelper
{

	public static function zaa()
	{
		return 'z';
	}

	/**
	 * String convert case
	 *
	 * Converts text case and returns it
	 *
	 * @param string                $str  Text for operation
	 * @param CASE_LOWER|CASE_UPPER $case New case
	 *
	 * @return string
	 */
	public static function convertCase($str, $case)
	{
		if ($case == CASE_LOWER)
			return self::lower($str);
		else
			return self::upper($str);
	}

	/**
	 * Converts turkish characters into latin ones
	 *
	 * @param $str
	 *
	 * @return mixed
	 */
	public static function convertEnglish($str)
	{
		$str = TurkishUtf8::str_ireplace([
			'ğ',
			'ü',
			'ş',
			'ı',
			'ö',
			'ç',
		], [
			'g',
			'u',
			's',
			'i',
			'o',
			'c'
		], $str);

		return $str;
	}

	/**
	 * Converts all non ascii characters to ascii ones
	 *
	 * @param $str
	 *
	 * @return bool|string
	 */
	public static function convertAscii($str)
	{
		return Utf8::toAscii($str);
	}

	/**
	 * Check string equality in case insensitive
	 *
	 * You can use this for checking string value like 'true'
	 *
	 * @param        $left
	 * @param string $right
	 *
	 * @return bool
	 */
	public static function equals($left, $right)
	{
		return self::convertCase($left, CASE_LOWER) == self::convertCase($right, CASE_LOWER) ? true : false;
	}

	/**
	 * Converts string boolean values into boolean type
	 *
	 * Supports only traditional 'true', 'false' strings
	 *
	 * @param $str
	 *
	 * @return bool
	 */
	public static function boolean($str)
	{
		return self::equals($str, 'true');
	}

	public static function randomUniqueString($length = 30) {
		if($length < 30)
			$length = 30;
		$tt = strval(DI::numberOfSecondsSince2000());
		$remains = $length - strlen($tt);
		return substr(base64_encode($tt . self::random($remains)), 0, $length);
	}


} 