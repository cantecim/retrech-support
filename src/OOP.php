<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 2.5.2015
 * Time: 23:02
 */

namespace Retrech\Support;


class OOP {

	/**
	 * Get caller function
	 *
	 * Returns the caller function name
	 *
	 * @return mixed
	 */
	public static function callerFunction()
	{
		$trace = debug_backtrace(3);
		$caller = $trace[ 2 ];

		return $caller[ 'function' ];
	}

} 