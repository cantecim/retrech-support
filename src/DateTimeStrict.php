<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 10.07.15
 * Time: 23:47
 */

namespace Retrech\Support;


use Carbon\Carbon;
use DateTime;

/**
 * Class DateTimeStrict
 *
 * Borrowed from Retrech\Support
 *
 * @package Retrech\Support
 */
trait DateTimeStrict
{

    /**
     * We just override to Eloquent's fromDateTime function
     *
     * We will set all datetime values to app timezone for storing it as
     *
     * @param \DateTime|int $value
     * @return string
     */
    public function fromDateTime($value)
    {
        /**
         * @var Carbon $dt
         */
        if ($value instanceof \DateTime) {
            $value = Carbon::instance($value);
        } else if (is_string($value)) {
            $value = new Carbon($value);
        }

        $timezone = \Config::get('app.timezone');
        $value->setTimezone($timezone);

        return parent::fromDateTime($value);
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  DateTime $date
     * @return string
     */
    protected function serializeDate(DateTime $date)
    {
        $carbon = new Carbon(
            $date->format('Y-m-d H:i:s.u'), $date->getTimeZone()
        );
        return $carbon->format(Carbon::ISO8601);
    }

    /**
     * Return date field as string
     *
     * @param string $field
     * @return string
     */
    public function dateField($field)
    {
        return $this->serializeDate($this->{$field});
    }

    /**
     * Return date field as string with specified timezone
     *
     * @param string $field
     * @param \DateTimeZone|string $timezone
     * @return string
     */
    public function dateFieldTimezone($field, $timezone)
    {
        /**
         * @var Carbon $date
         */
        $date = $this->{$field};
        $date->timezone = $timezone;
        return $this->serializeDate($date);
    }

}