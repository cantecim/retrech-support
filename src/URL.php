<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 5.5.2015
 * Time: 15:49
 */

namespace Retrech\Support;


class URL {

	/**
	 * Parses given URL
	 *
	 * @see parse_url
	 *
	 * @param     $url
	 * @param int $component
	 *
	 * @return mixed
	 */
	public static function parseUrl($url, $component = -1)
	{
		return parse_url($url, $component);
	}

	/**
	 * Get query part of an url as an array
	 *
	 * @param $url
	 *
	 * @return array
	 */
	public static function getQuery($url)
	{
		$ret = [ ];
		parse_str(parse_url($url, PHP_URL_QUERY), $ret);
		return $ret;
	}

	/**
	 * Modify query part of an url
	 *
	 * @param       $url
	 * @param array $query
	 *
	 * @return string
	 */
	public static function setQuery($url, array $query)
	{
		$url = self::parseUrl($url);
		$url['query'] = self::buildQueryString($query);

		return self::buildUrl($url);
	}

	/**
	 * Builds query string from an array
	 *
	 * @param array $query
	 * @param bool  $encoded
	 *
	 * @return string
	 */
	public static function buildQueryString(array $query, $encoded = false)
	{
		if($encoded)
			return http_build_query($query);

		array_walk($query, function(&$v, $k) {
			$v = $k . '=' . $v;
		});
		$query = array_values($query);

		return implode('&', $query);
	}

	/**
	 * Builds URL
	 *
	 * $url param needs to be same format as parse_url returns
	 * @see parse_url
	 * @param array $url
	 *
	 * @return string
	 */
	public static function buildUrl(array $url)
	{
		if(!isset($url['scheme']) && !isset($url['host']))
			return '';

		$scheme = $url['scheme'];
		$host = $url['host'];
		$port = @$url['port'];
		$user = @$url['user'];
		$pass = @$url['pass'];
		$path = @$url['path'];
		$query = @$url['query'];
		$fragment = @$url['fragment'];

		$composed = $scheme . '://';
		if($user !== NULL && $pass !== NULL)
			$composed .= $user . ':' . $pass . '@';

		$composed .= $host;
		if($port !== NULL)
			$composed .= ':' . $port;
		if($path !== NULL)
			$composed .= $url['path'];
		if($query !== NULL)
			$composed .= '?' . $url['query'];
		if($fragment !== NULL)
			$composed .= '#' . $fragment;

		return $composed;
	}

} 