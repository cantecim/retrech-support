<?php
/**
 * Created by PhpStorm.
 * User: cantecim
 * Date: 24.06.15
 * Time: 21:10
 */

namespace Retrech\Support;


/**
 * Class EnumHolder
 *
 * Borrowed from Retrech\Support
 *
 * @package Retrech\Support
 */
class EnumHolder
{

	/**
	 * @var array enum holder array
	 */
	private static $enums = [ ];

	/**
	 * @var array topic that are group
	 */
	private static $groups = [ ];

	/**
	 * Get only enum values from topic
	 *
	 * @param string      $topic
	 * @param string|null $group
	 *
	 * @return array
	 */
	public static function getEnumValues($topic, $group = NULL, $withKeys = false)
	{
		if (isset(self::$enums[ $topic ])) {
			if (in_array($topic, self::$groups)) {
				// its a group concatenate all values under groups
				$values = [ ];
				if ($group !== NULL && isset(self::$enums[ $topic ][ $group ])) {
					$values = self::$enums[ $topic ][ $group ];
					if (!$withKeys)
						$values = array_values($values);
				} else {
					foreach (self::$enums[ $topic ] as $g => $enums) {
						if ($group !== NULL) {
							$groups = explode('-', $g);
							if (in_array($group, $groups)) {
								if (!$withKeys)
									$values = array_values($enums);
								else
									$values = $enums;
								break;
							}
						} else {
							if (!$withKeys)
								$enums = array_values($enums);
							$values = array_merge($values, $enums);
						}
					}
				}

				if (!count($values))
					$values = false;

				return $values;
			} else {
				$values = self::$enums[ $topic ];
				if (!$withKeys)
					$values = array_values($values);

				return $values;
			}
		} else
			return false;
	}

	/**
	 * Get a value with topic and key
	 *
	 * @param      $topic
	 * @param      $key
	 * @param null $group
	 *
	 * @return mixed
	 */
	public static function getEnumValue($topic, $key, $group = NULL)
	{
		$values = self::getEnumValues($topic, $group, true);
		if ($values !== false) {
			if (isset($values[ $key ]))
				$values = $values[ $key ];
			else
				$values = false;
		}

		return $values;
	}

	/**
	 * Get enum values for using in validation rules from topic
	 *
	 * @param $topic
	 *
	 * @return string
	 */
	public static function getEnumValuesForValidationRule($topic)
	{
		$values = self::getEnumValues($topic);
		if ($values === false)
			return 'N/A';

		return implode(',', $values);
	}

	/**
	 * Get only enum keys from topic
	 *
	 * @param $topic
	 *
	 * @return array
	 */
	public static function getEnumKeys($topic)
	{
		if (isset(self::$enums[ $topic ]))
			if (in_array($topic, self::$groups)) {
				// its a group concatenate all key under groups
				$values = [ ];
				foreach (self::$enums[ $topic ] as $group => $enums) {
					$values = array_merge($values, array_keys($enums));
				}

				return $values;
			} else
				return array_keys(self::$enums[ $topic ]);
		else
			return false;
	}

	/**
	 * Get only groups from topic
	 *
	 * @param $topic
	 *
	 * @return array|bool
	 */
	public static function getEnumGroups($topic)
	{
		$enum = self::getEnum($topic);
		if ($enum !== false && in_array($topic, self::$groups)) {
			$enum = array_keys($enum);
		} else
			return false;

		return $enum;
	}

	/**
	 * Get enums for a specifiec topic
	 *
	 * @param $topic
	 *
	 * @return bool
	 */
	public static function getEnum($topic)
	{
		if (isset(self::$enums[ $topic ]))
			return self::$enums[ $topic ];
		else
			return false;
	}

	/**
	 * Register a topic
	 *
	 * It will override if topic exists
	 * $enums has to be enum keys and values array like below;
	 * [
	 *        'male' => 'M',
	 *        'female' => 'F'
	 * ]
	 *
	 * @param $topic
	 * @param $enums
	 */
	public static function register($topic, $enums, $isGroup = false)
	{
		self::$enums[ $topic ] = $enums;

		if ($isGroup)
			array_push(self::$groups, $topic);
	}

	/**
	 * Un-register topic
	 *
	 * Removes specified topic from holding
	 *
	 * @param $topic
	 */
	public static function unregister($topic)
	{
		unset(self::$enums[ $topic ]);
	}

	/**
	 * Check for equality of enum with given value
	 *
	 * @param $topic
	 * @param $key
	 * @param $value
	 *
	 * @return bool
	 */
	public static function equal($topic, $key, $value)
	{
		if (!isset(self::$enums[ $topic ][ $key ]))
			return false;

		return self::$enums[ $topic ][ $key ] == $value;
	}

	/**
	 * Check for equality of enum with given value in case insensitive
	 *
	 * @param $topic
	 * @param $key
	 * @param $value
	 *
	 * @return bool
	 */
	public static function equalIgnoreCase($topic, $key, $value)
	{
		if (!isset(self::$enums[ $topic ][ $key ]))
			return false;

		return Str::equals(self::$enums[ $topic ][ $key ], $value);
	}

}