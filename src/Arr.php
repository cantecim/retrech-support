<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 2.5.2015
 * Time: 22:42
 */

namespace Retrech\Support;


use Illuminate\Support\Arr as BaseHelper;

class Arr extends BaseHelper
{

	/**
	 * Change array key's case
	 *
	 * Changes the array key's case with specified deeper level
	 *
	 * @param array $arr       Array for operation
	 * @param int   $case      New case
	 * @param int   $deeplevel How many level we will go into deeper ?
	 *
	 * @return array
	 */
	public static function keyCase(array $arr, $case = CASE_LOWER, $deeplevel = 1)
	{
		if (!is_array($arr))
			return false;
		$ret = [ ];
		foreach ($arr as $k => $v) {
			if (($deeplevel > 1) && is_array($v)) {
				$v = self::keyCase($v, CASE_LOWER, $deeplevel - 1);
			}
			$ret[ Str::convertCase($k, $case) ] = $v;
		}

		return $ret;
	}

	/**
	 * Replace array key
	 *
	 * Searches for specific array key and replaces it
	 *
	 * @param array  $arr        Array for operation
	 * @param string $searchKey  The text that being replaced
	 * @param string $replaceKey The text that will placed over search key
	 *
	 * @return array
	 */
	public static function keyReplace(array $arr, $searchKey, $replaceKey)
	{
		if (isset($arr[ $searchKey ])) {
			$arr[ $replaceKey ] = $arr[ $searchKey ];
			unset($arr[ $searchKey ]);
		}

		return $arr;
	}

	/**
	 * Replaces all whitespaces for all values
	 *
	 * Does not goes into deeper only removes root levels
	 *
	 * @param array $arr
	 *
	 * @return array
	 */
	public static function valuesRemoveWhitespaces(array $arr)
	{
		foreach ($arr as $key => $value)
			$arr[ $key ] = str_replace(' ', '', $value);

		return $arr;
	}

} 