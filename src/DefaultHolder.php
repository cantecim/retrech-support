<?php
/**
 * Created by PhpStorm.
 * User: cantecim
 * Date: 25.06.15
 * Time: 21:42
 */

namespace Retrech\Support;


/**
 * Class DefaultHolder
 *
 * Borrowed from Retrech\Support
 *
 * @package Retrech\Support
 */
class DefaultHolder
{

	/**
	 * Default value holder array
	 *
	 * @var array
	 */
	private static $defaults = [ ];

	/**
	 * Register a default value
	 *
	 * @param $key
	 * @param $value
	 */
	public static function register($key, $value)
	{
		Arr::set(self::$defaults, $key, $value);
	}

	/**
	 * Un-register a key
	 *
	 * @param $key
	 *
	 * @return mixed
	 */
	public static function unregister($key)
	{
		if (isset(self::$defaults[ $key ])) {
			return Arr::pull(self::$defaults, $key);
		}

		$keys = explode('.', $key);
		$arr = &self::$defaults;
		foreach ($keys as $i => $key) {
			if (!is_array($arr) || !array_key_exists($key, $arr))
				return NULL;

			if(count($keys) - 1 == $i)
				unset($arr[$key]);
			else
				$arr = &$arr[$key];
		}

		return $arr;
	}

	/**
	 * Get default value for a key
	 *
	 * @param $key
	 *
	 * @return mixed
	 */
	public static function get($key)
	{
		return Arr::get(self::$defaults, $key);
	}

}