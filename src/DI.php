<?php
/**
 * Created by can.tecim@gmail.com
 * Developed by can.tecim@gmail.com
 * Date: 2.5.2015
 * Time: 22:31
 */

namespace Retrech\Support;


class DI {

	/**
	 * Calculates elapsed seconds from DateInterval
	 *
	 * @param \DateInterval $diff DateInterval instance that will be worked on
	 *
	 * @return mixed
	 */
	public static function elapsedSeconds(\DateInterval $diff)
	{
		return ($diff->days * 86400) + ($diff->h * 3600) + ($diff->i * 60) + $diff->s;
	}

	/**
	 * Calculates elapsed minutes from DateInterval
	 *
	 * @param \DateInterval $diff DateInterval instance that will be worked on
	 *
	 * @return mixed
	 */
	public static function elapsedMinutes(\DateInterval $diff)
	{
		return ($diff->days * 86400) + ($diff->h * 3600) + ($diff->i * 60);
	}

	/**
	 * Calculates elapsed hours from DateInterval
	 *
	 * @param \DateInterval $diff DateInterval instance that will be worked on
	 *
	 * @return mixed
	 */
	public static function elapsedHours(\DateInterval $diff)
	{
		return ($diff->days * 86400) + ($diff->h * 3600);
	}

	/**
	 * Calculates elapsed seconds automatically
	 *
	 * Uses the given Datetime instances to get difference and then calculates
	 * elapsed seconds for that DateInterval
	 *
	 * @param \DateTime $past    DateTime object for past
	 * @param \DateTime $feature DateTime object for feature
	 *
	 * @return mixed
	 */
	public static function calculateTimeIntervalInSeconds(\DateTime $past, \DateTime $feature)
	{
		return self::elapsedSeconds($feature->diff($past));
	}

	/**
	 * Number of seconds since 2000
	 *
	 * Returns number of seconds that have elapsed since 2000-01-01 00:00:00 time.
	 *
	 * @return mixed
	 */
	public static function numberOfSecondsSince2000()
	{
		return self::calculateTimeIntervalInSeconds(new \DateTime('2000-01-01 00:00:00', new \DateTimeZone('UTC')), new \DateTime('now', new \DateTimeZone('UTC')));
	}

} 